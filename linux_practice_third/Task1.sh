#!/bin/bash

mkdir task1
cd ./task1
for ((a=0; a<366; a++))
do
touch `date +%F -d "+$a days"`.csv
echo 'cite; country; date; views; clicks'>>`date +%F -d "+$a days"`.csv
views=$RANDOM
let "views %= 10000"
clicks=$RANDOM
let "clicks %= 10"
echo "www.abc.com; USA; `date +%F -d "+$a days"`; $views; $clicks">>`date +%F -d "+$a days"`.csv
views=$RANDOM
let "views %= 10000"
clicks=$RANDOM
let "clicks %= 10"
echo "www.cba.com; France; `date +%F -d "+$a days"`; $views; $clicks">>`date +%F -d "+$a days"`.csv
done
