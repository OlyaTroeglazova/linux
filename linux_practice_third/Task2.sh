#!/bin/bash

cd ./task1
let "numberOfFiles = `find -type f | wc -l`"
if((numberOfFiles == 366))
then echo yes
else
echo no
fi
for((a=0; a<366; a++))
do
sed -e 's:\-:\/:g' `date +%F -d "+$a days"`.csv > tmpfile
 mv tmpfile `date +%F -d "+$a days"`.csv
awk '{print $3, $1, $2, $4, $5}' `date +%F -d "+$a days"`.csv  > tmpfile
 mv tmpfile `date +%F -d "+$a days"`.csv
done
