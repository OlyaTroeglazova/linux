#!/bin/bash

cd ./task1

for filename in `find * | grep -P  '(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)\.csv'`
do
newfile=$(date --date=`echo $filename | cut -c 1-10 ` "+%A").csv
if [ -e $newfile ]; then
awk 'NR!=1 {print $0}' $filename>> $newfile \
&& \
rm $filename
else
awk '{print $0}' $filename>> $newfile \
&& \
rm $filename
fi
done
